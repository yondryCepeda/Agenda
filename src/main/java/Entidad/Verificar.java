package Entidad;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServlet;

@ManagedBean
@RequestScoped
public class Verificar implements Serializable{
	private static final long serialVersionUID = 3203532318091763992L;
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistence");
	EntityManager em = emf.createEntityManager();
	@PostConstruct
	public void postConstruct(){
		
	}
	private List<Agenda> lista = em.createNamedQuery("Agenda.Personas", Agenda.class).getResultList();
	
	private String nombre;
	
	
	
	public List<Agenda> getLista() {
		return lista;
	}


	public void setLista(List<Agenda> lista) {
		this.lista = lista;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

		
	public String comparar(){
		
		
		List <Agenda> personas = em.createNamedQuery("Agenda.Personas", Agenda.class).getResultList();
		Agenda persona = em.find(Agenda.class, 1);
		System.out.println(personas);
		System.out.println(persona);
		
		
		if(nombre.equals("Maria")){
		return "positivo";
		}else{
			return "negativo";
		}
	}

}
